﻿<?php 

        $dm = new DOMDocument;
        $domF = new DomDocument('1.0','UTF-8'); 
        $dm->load('http://www.gotoknow.org/blogs/posts?format=rss');

        $rootDom = $dm->documentElement;
        $elem = $rootDom->getElementsByTagName('item');

        echo "Reading from summaryBlogs.xml<br>";
        $items = $domF->appendChild($domF->createElement('items'));

        foreach ($elem AS $item) {
                $titleOP = $item->getElementsByTagName("title")->item(0)->nodeValue;
                $linkOP = $item->getElementsByTagName("link")->item(0)->nodeValue;
                $authorOP = $item->getElementsByTagName("author")->item(0)->nodeValue;
                
                $item = $items->appendChild($domF->createElement('item')); 

                $title = $item->appendChild($domF->createElement('title')); 
                $title->appendChild( $domF->createTextNode($titleOP));
                $link = $item->appendChild($domF->createElement('link'));
                $link->appendChild( $domF->createTextNode($linkOP));
                $author = $item->appendChild($domF->createElement('author')); 
                $author->appendChild( $domF->createTextNode($authorOP));

                echo $linkOP . $authorOP;
        }

        $domF->formatOutput = true;
        $test = $domF->saveXML();
        $domF->save('summaryBlogs.xml'); 

?>