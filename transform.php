﻿<?php

        $xslFile = "SummaryBlogs.xsl";
        $xmlFile = "SummaryBlogs.xml";
        $docDom = new DOMDocument();
        $xsl = new XSLTProcessor();

        $docDom->load($xslFile);
        $xsl->importStyleSheet($docDom);
        $docDom->load($xmlFile);
        
        echo $xsl->transformToXML($docDom);
        
?>